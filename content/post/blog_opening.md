+++
author = "Clément Le Ludec"
title = "Hello world!"
date = "2021-10-08"
description = "Article sur l'ouverture de ce site."
tags = [
    "hello",
    "opening",
]
categories = [
    "introduction"
]
+++

Cet article ne sert à rien d'autre qu'à marquer l'ouverture de ce site. L'objectif est de présenter mes travaux de chercheur. J'utiliserai le blog occasionnellement afin d'annoncer des conférences, mais aussi pour raconter faire un carnet de terrain de ma thèse.
<!--more-->

