---
date: 2019-05-28
type: section
layout: "publications"
---
## Journal Publications

1.  Paola Tubaro, Marion Coville, **Clément Le Ludec** and Antonio A.
    Casilli. Hidden inequalities: the gendered labour of women on
    micro-tasking platforms In *Internet Policy Review*, *To be
    published*

2.  Maxime Cornet, **Clément Le Ludec**, Elinor Wahal and Mandie Joulin.
    Beyond “platformisation”: Designing a mixed-methods approach to
    inspect (digital) working conditions through organizational systems?
    Work Organisation, Labour & Globalisation. *To be published*

3.  **Clément Le Ludec**, Elinor Wahal, Antonio A. Casilli and Paola
    Tubaro. Quel statut pour les « petits doigts » de l’intelligence
    artificielle ? Présent et perspectives du micro-travail en France.
    In *Les Mondes du travail*, October 2020

4.  Paola Tubaro, **Clément Le Ludec** and Antonio A. Casilli. Counting
    ‘micro-workers’: societal and methodological challenges around new
    forms of labour. In *Work Organisation, Labour & Globalisation*,
    September 2020

5.  Antonio Casilli, Paola Tubaro, **Clément Le Ludec** and Elinor
    Wahal. En la trastienda de la inteligencia artificial. Une
    investigacion sobre las plataformas de micro-trabajo en Francia. In
    *Arxius*, December 2019

## Conferences

1.  **Clément Le Ludec** and Maxime Cornet. Deplatformizing data labour
    ? An empirical study of French AI companies organisation. In *BSA
    Work, Employment and Society 2021*. August 2021

2.  **Clément Le Ludec**. The fabric of AI systems: which organisation
    for workers and suppliers of automation ?. In *EGOS 2021*. July 2021

3.  **Clément Le Ludec** and Mandie Joulin. Le micro-travail dans la
    chaîne de valeur de l’intelligence artificielle. Quelle place pour
    les pays émergents et les pays en voie de développement ?. In
    *ComtecDev 2021*. June 2021

## Research reports

1.  Antonio A. Casilli, Paola Tubaro, **Clément Le Ludec**, Marion
    Coville, Maxime Besenval, Touhfat Mouhtare, Elinor Wahal. Micro-work
    in France : beyond automation, new forms of precarious labour. Final
    report of the DIPLab’s project. May 2019

## Policy reports

1.  **Conseil national du numérique**. Travail à l’ère des plateformes :
    mise à jour requise. Jun. 2020

2.  **Conseil national du numérique**. L’accessibilité numérique : une
    obligation envers les citoyens, un levier stratégique pour les
    acteurs. Feb. 2020

3.  **Conseil national du numérique**. Rapport pour le secrétaire d’État
    au Numérique sur les “États généraux des nouvelles régulations
    numériques" sur la protection sociale des travailleurs des
    plateformes. Mai 2020

4.  **Conseil national du numérique**. Avis relatif au projet de décret
    sur l’accessibilité numérique des services publics en ligne. 2019

## Workshops

1.  Antonio Casilli, Maxime Cornet and **Clément Le Ludec**. Le travail
    de l’intelligence artificielle : L’externalisation du travail
    d’annotation des données dans les pays d’Afrique francophone. In
    *Séminaire Sciences sociales sur un plateau*. May 2021

2.  **Clément Le Ludec** and Maxime Cornet. Le travail de l’intelligence
    artificielle : vers la fin des plateformes numériques de travail et
    du taylorisme numérique? In *Journée d’études - Cultures numériques
    de l’Université de Nantes*. May 2021

3.  **Clément Le Ludec**. Comment théoriser le micro-travail ?
    *Séminaire de recherche - Flashpoint*, Mar 2021

4.  **Clément Le Ludec**. Les enjeux éthiques du travail du clic. École
    d’hiver sur l’éthique de la technogie. *Université de Saint-Paul
    (Ottawa)*. Jan 2021

5.  **Clément Le Ludec**. Paysage des plateformes et du digital labor.
    *Les explorateurs du numérique (Région Île-de-France)*. Jun 2021

6.  **Clément Le Ludec**. La France avec les robots. (Micro)-travail en
    start-up nation. Doctoriales de *l’Institut Interdisciplinaire de
    l’Innovation*. May 2021

7.  **Clément Le Ludec**. The state of digital accessibility in France.
    *European Union - Web accessibility Directive Expert Group*. June
    2020

8.  **Clément Le Ludec**. Faire face à la trasformation du travail.
    *Numérique en commun*. Oct. 2019

9.  **Clément Le Ludec**. Presentation of DIPLab’s final report. *France
    Stratégie*. June 2019

## Public policies audition

1.  Paola Tubaro and **Clément Le Ludec**, RAPPORT D’INFORMATION fait au
    nom de la mission d’information sur : « l’uberisation de la société
    : quel impact des plateformes numériques sur les métiers et l’emploi
    ? » Par M. Pascal SAVOLDELLI, 29 septembre 2021

