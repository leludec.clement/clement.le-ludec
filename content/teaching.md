## 2021-2022
- **Aivancity**, Sociology of AI (12 hours - business / engineers school)  
## 2020-2021

#### Formations courtes
- **ESIEE Paris**, Le travail à l’ère numérique (3 hours - engineers)

 - **Institut de la gestion publique et du développement économique (IGPDE)**    Intelligence artificielle et action publique (2 hours - high civil servant) 

 - **EAC**, Accessibilité numérique (2 hours - art student) 
#### Enseignements
- **Université Catholique de l'Ouest (UCO)**, CM Sociologie du numérique (18 hours - M1) 
- **Université Catholique de l'Ouest (UCO)**, CM Méthodologie d’analyse des données web avec R
(12 hours - M2) 
- **Université Catholique de l'Ouest (UCO)**, TD Cultures numériques (27 hours - L1) 
- **Université Catholique de l'Ouest (UCO)**, TD Méthodologie de la recherche (6 hours -
M1) 
- **Télécom Paris**, TD SHS appliquées au monde des start-up
(26 hours - M1) 
#### Encadrement de mémoire
- **Université Catholique de l'Ouest (UCO)**, un mémoire sur l’UX et un mémoire sur la consommation de visites virtuelles dans les musées (M1) 
