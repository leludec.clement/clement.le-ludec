+++
title = "About"
description = "Hugo, the world's fastest framework for building websites"
date = "2019-02-28"
aliases = ["about-us", "about-hugo", "contact"]
author = "Hugo Authors"
+++
### A propos de moi
Je suis doctorant en sociologie du numérique et en sociologie du travail à l'Institut Polytechnique de Paris (IPP), au sein du département Sciences Économiques et Sociales (SES) de Télécom Paris. Je suis membre de l'équipe Digital Platform Labour ([DiPLab](http://diplab.eu)).

J'étudie l'externalisation de la production de l'annotation des données nécessaires au machine learning, dans le cadre du [projet HUSH](https://anr.fr/Projet-ANR-19-CE10-0012) financé par l'Agence Nationale de la Recherche (ANR). Avant cela, j'ai également travaillé au Conseil National du Numérique (CNNum) sur les politiques publiques du travail de plateforme et sur l'accessibilité numérique pour personnes en situation de handicap.

En parralèle de ma thèse, je suis membre du Conseil National Consultatif des Personnes Handicapées (CNCPH) et je suis représentant des doctorant de Télécom Paris auprès du Conseil de l'école et du conseil de la recherche, ainsi que représentant des doctorants affilié à la CFDT au conseil d'administration de l'Institut Mines Télécom (IMT).

Pour plus d'informations, {{% staticref "static/uploads/2021_05_11___CV_CLL.pdf" %}}télécharger mon CV{{% /staticref %}}
